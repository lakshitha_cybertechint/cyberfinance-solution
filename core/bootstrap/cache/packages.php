<?php return array (
  'brotzka/laravel-dotenv-editor' => 
  array (
    'providers' => 
    array (
      0 => 'Brotzka\\DotenvEditor\\DotenvEditorServiceProvider',
    ),
    'aliases' => 
    array (
      'DotenvEditor' => 'Brotzka\\DotenvEditor\\DotenvEditorFacade',
    ),
  ),
  'cartalyst/sentinel' => 
  array (
    'providers' => 
    array (
      0 => 'Cartalyst\\Sentinel\\Laravel\\SentinelServiceProvider',
    ),
    'aliases' => 
    array (
      'Activation' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Activation',
      'Reminder' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Reminder',
      'Sentinel' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Sentinel',
    ),
  ),
  'elibyy/tcpdf-laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Elibyy\\TCPDF\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'PDF' => 'Elibyy\\TCPDF\\Facades\\TCPDF',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'srmklive/paypal' => 
  array (
    'providers' => 
    array (
      0 => 'Srmklive\\PayPal\\Providers\\PayPalServiceProvider',
    ),
    'aliases' => 
    array (
      'PayPal' => 'Srmklive\\PayPal\\Facades\\PayPal',
    ),
  ),
);