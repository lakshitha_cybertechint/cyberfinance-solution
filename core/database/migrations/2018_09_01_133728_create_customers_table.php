<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('customers', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('shop_name');
		    $table->string('address1');
		    $table->string('address2')->nullable();
		    $table->string('street')->nullable();
		    $table->string('region');
		    $table->string('geo_location')->nullable();
		    $table->integer('route'); // route_id
		    $table->string('owner');
		    $table->string('contact');
		    $table->string('category');
		    $table->text('notes')->nullable();
		    $table->integer('payment'); // Cash=1 / Credits=2 / Cheque=3
		    $table->string('duration')->nullable();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
