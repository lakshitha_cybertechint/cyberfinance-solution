<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('products', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->string('code');
		    $table->text('description')->nullable();
		    $table->double('bought_price');
		    $table->double('selling_price');
		    $table->double('mr_price')->nullable();
		    $table->integer('current_stock')->nullable();
		    $table->string('category');
		    $table->string('sub_category');
		    $table->string('image')->nullable();
		    $table->tinyInteger('isActive')->default(1);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
