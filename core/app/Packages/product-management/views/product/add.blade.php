@extends('layouts.back.master') @section('current_title','New product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('branch/list')}}">Product Management</a></li>

            <li class="active">
                <span>New Product</span>
            </li>
        </ol>
    </div>
@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form method="POST" class="form-horizontal" id="form">
                        {!!Form::token()!!}

                        <div class="form-group"><label class="col-sm-2 control-label">PRODUCT NAME</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">PRODUCT CODE</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="code"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="description"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">BOUGHT PRICE</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="bought_price"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">SELLING PRICE</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="selling_price"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">MRP</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="mr_price"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CURRENT STOCK</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="current_stock"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category" id="">
                                    <option value="">Cat 1</option>
                                    <option value="">Cat 2</option>
                                    <option value="">Cat 3</option>
                                    <option value="">Cat 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">SUB CATEGORY</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="sub_category" id="">
                                    <option value="">Cat 1</option>
                                    <option value="">Cat 2</option>
                                    <option value="">Cat 3</option>
                                    <option value="">Cat 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">IMAGE</label>
                            <div class="col-sm-10"><input type="file" class="form-control" name="image"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Active</label>
                            <div class="col-sm-10">
                                <input type="radio" class="" name="isActive" value="1"> Active <br>
                                <input type="radio" class="" name="isActive" value="0"> Inactive
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                                </button>
                                <button class="btn btn-primary" type="submit">Done</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @stop
        @section('js')
            <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $(".js-source-states").select2();

                    $("#form").validate({
                        rules: {
                            name: {
                                required: true

                            },
                            code: {
                                required: true
                            },
                            bought_price: {
                                required: true
                            },
                            selling_price: {
                                required: true
                            }
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });


            </script>
@stop