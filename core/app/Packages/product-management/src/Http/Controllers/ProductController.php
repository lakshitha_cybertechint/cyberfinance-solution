<?php

namespace ProductManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ProductManage\Http\Requests\ProductRequest;
use ProductManage\Models\Product;
use Sentinel;
use Response;


class ProductController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Product Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}

	/**
	 * Show the Product add screen to the user.
	 *
	 * @return Response
	 */
	public function addView() {
		return view( 'productManage::product.add' )->with( [] );
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to product add
	 */
	public function add( ProductRequest $request ) {

		$count = Product::where( 'name', '=', $request->get( 'name' ) )->count();
		if ( $count == 0 ) {
			Product::Create( array(
					'name'          => $request->input( 'name' ),
					'code'          => $request->input( 'code' ),
					'description'   => $request->input( 'description' ),
					'bought_price'  => $request->input( 'bought_price' ),
					'selling_price' => $request->input( 'selling_price' ),
					'mr_price'      => $request->input( 'mr_price' ),
					'current_stock' => $request->input( 'current_stock' ),
					'category'      => $request->input( 'category' ),
					'sub_category'  => $request->input( 'sub_category' ),
					'image'         => $request->input( 'image' ),
					'isActive'      => $request->input( 'isActive' )
				)
			);

			return redirect( 'product/add' )->with( [
				'success'         => true,
				'success.message' => 'Product Created successfully!',
				'success.title'   => 'Well Done!'
			] );
		} else {

			return redirect( 'product/add' )->with( [
				'error'         => true,
				'error.message' => 'Product Already Exist!',
				'error.title'   => 'Duplicate!'
			] );
		}


	}

	/**
	 * View product List View
	 *
	 * @return Response
	 */
	public function listView() {
		return view( 'productManage::product.list' );
	}

	/**
	 * product list
	 *
	 * @return Response
	 */
	public function jsonList( Request $request ) {
		if ( $request->ajax() ) {
			$data     = Product::all();
			$jsonList = array();
			$i        = 1;
			foreach ( $data as $key => $product ) {

				$dd = array();
				array_push( $dd, $i );

				if ( $product->name != "" ) {
					array_push( $dd, $product->name );
				} else {
					array_push( $dd, "-" );
				}
				if ( $product->current_stock != "" ) {
					array_push( $dd, $product->current_stock );
				} else {
					array_push( $dd, "-" );
				}
				if ( $product->isActive != "" ) {
					array_push( $dd, $product->isActive );
				} else {
					array_push( $dd, "-" );
				}

				$permissions = Permission::whereIn( 'name', [
					'product.edit',
					'admin'
				] )->where( 'status', '=', 1 )->lists( 'name' );
				if ( Sentinel::hasAnyAccess( $permissions ) ) {
					array_push( $dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url( 'product/edit/' . $product->id ) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>' );
				} else {
					array_push( $dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>' );
				}

				$permissions = Permission::whereIn( 'name', [
					'product.delete',
					'admin'
				] )->where( 'status', '=', 1 )->lists( 'name' );
				if ( Sentinel::hasAnyAccess( $permissions ) ) {
					array_push( $dd, '<center><a href="#" class="product-delete" data-id="' . $product->id . '" data-toggle="tooltip" data-placement="top" title="Delete product"><i class="fa fa-trash-o"></i></a></center>' );
				} else {
					array_push( $dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>' );
				}

				array_push( $jsonList, $dd );
				$i ++;
			}

			return Response::json( array( 'data' => $jsonList ) );
		} else {
			return Response::json( array( 'data' => [] ) );
		}
	}


	/**
	 * Activate or Deactivate user
	 *
	 * @param  Request $request branch id with status to change
	 *
	 * @return json object with status of success or failure
	 */
	public function status( Request $request ) {
		if ( $request->ajax() ) {
			$id     = $request->input( 'id' );
			$status = $request->input( 'status' );

			$branch = Branch::find( $id );
			if ( $branch ) {
				$branch->status = $status;
				$branch->save();

				return response()->json( [ 'status' => 'success' ] );
			} else {
				return response()->json( [ 'status' => 'invalid_id' ] );
			}
		} else {
			return response()->json( [ 'status' => 'not_ajax' ] );
		}
	}

	/**
	 * Delete a branch
	 *
	 * @param  Request $request branch id
	 *
	 * @return Json            json object with status of success or failure
	 */
	public function delete( Request $request ) {
		if ( $request->ajax() ) {
			$id = $request->input( 'id' );

			$product = Product::find( $id );
			if ( $product ) {
				$product->delete();

				return response()->json( [ 'status' => 'success' ] );
			} else {
				return response()->json( [ 'status' => 'invalid_id' ] );
			}
		} else {
			return response()->json( [ 'status' => 'not_ajax' ] );
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView( $id ) {
		$curproduct = Product::find( $id );
		if ( $curproduct ) {
			return view( 'productManage::product.edit' )->with( [
				'curProduct' => $curproduct,

			] );
		} else {
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit( ProductRequest $request, $id ) {


		$count = Product::where( 'id', '!=', $id )->where( 'name', '=', $request->get( 'name' ) )->count();

		if ( $count == 0 ) {
			$productOld       = Product::where( 'id', $id )->take( 1 )->get();
			$branch          = $productOld[0];
			$branch->name    = $request->get( 'name' );
			$branch->code    = $request->get( 'code' );
			$branch->description    = $request->get( 'description' );
			$branch->bought_price    = $request->get( 'brought_price' );
			$branch->selling_price    = $request->get( 'selling_price' );
			$branch->mr_price    = $request->get( 'mr_price' );
			$branch->current_stock    = $request->get( 'current_stock' );
			$branch->category    = $request->get( 'category' );
			$branch->sub_category    = $request->get( 'sub_category' );
			$branch->image    = $request->get( 'image' );
			$branch->save();

			return redirect( 'product/edit/' . $id )->with( [
				'success'         => true,
				'success.message' => 'Product updated successfully!',
				'success.title'   => 'Good Job!'
			] );
		} else {
			return redirect( 'product/edit/' . $id )->with( [
				'error'         => true,
				'error.message' => 'Product Already Exist!',
				'error.title'   => 'Duplicate!'
			] );
		}


	}
}
