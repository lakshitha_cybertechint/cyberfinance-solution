<?php
/**
 * Product MANAGEMENT ROUTES
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'product', 'namespace' => 'ProductManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'product.add', 'uses' => 'ProductController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'product.edit', 'uses' => 'ProductController@editView'
      ]);

      Route::get('list', [
        'as' => 'product.list', 'uses' => 'ProductController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'product.list', 'uses' => 'ProductController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'product.add', 'uses' => 'ProductController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'product.edit', 'uses' => 'ProductController@edit'
      ]);

      Route::post('status', [
        'as' => 'product.status', 'uses' => 'ProductController@status'
      ]);

      Route::post('delete', [
        'as' => 'product.delete', 'uses' => 'ProductController@delete'
      ]);
    });
});