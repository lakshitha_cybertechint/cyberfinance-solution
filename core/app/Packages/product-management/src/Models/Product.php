<?php

namespace ProductManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Product Model Class
 */
class Product extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'code',
		'description',
		'bought_price',
		'selling_price',
		'mr_price',
		'current_stock',
		'category',
		'sub_category',
		'image',
		'isActive'
	];


}
