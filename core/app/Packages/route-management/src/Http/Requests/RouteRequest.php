<?php
namespace RouteManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class RouteRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){    

	
		$rules = [
			'name' => 'required',			
		];
	
		
		return $rules;
	}

}
