<?php
namespace RouteManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use RouteManage\Http\Requests\RouteRequest;
use RouteManage\Models\Route;
use Sentinel;
use Response;



class RouteController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Route Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		return view( 'routeManage::route.add')->with([]);
	}

	/**
	 * Add new Route data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function add(RouteRequest $request)
	{
	
		$count= Route::where('name', '=',$request->get('name' ))->count();
		if ($count==0) {
			Route::Create(array(
				'name' => $request->input('name'),
				'day' => $request->input('day'),
				'target' => $request->input('target')
				)
			);		

			return redirect('route/add')->with([ 'success' => true,
				'success.message'=> 'Route Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('route/add')->with([ 'error' => true,
				'error.message'=> 'Route Already Exist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'routeManage::route.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Route::all();
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $route) {
				
				$dd = array();
				array_push($dd, $i);
				
				if($route->name != ""){
					array_push($dd, $route->name);
				}else{
					array_push($dd, "-");
				}
				if($route->day != ""){
					array_push($dd, $route->day);
				}else{
					array_push($dd, "-");
				}
				if($route->target != ""){
					array_push($dd, $route->target);
				}else{
					array_push($dd, "-");
				}

				$permissions = Permission::whereIn('name',['route.edit','admin'])->where('status','=',1)->pluck('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('route/edit/'.$route->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit Route"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['route.delete','admin'])->where('status','=',1)->pluck('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="route-delete" data-id="'.$route->id.'" data-toggle="tooltip" data-placement="top" title="Delete Route"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request branch id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$route = Route::find($id);
			if($route){
				$route->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curroute=Route::find($id);
		if($curroute){
			return view( 'routeManage::route.edit' )->with([
				'curRoute' => $curroute,

				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(RouteRequest $request, $id)
	{
		
		
		$count= Route::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		
			if($count==0){
				$routeOld =  Route::where('id',$id)->take(1)->get();
				$route=$routeOld[0];
				$route->name = $request->get('name');
				$route->day = $request->get('day');
				$route->target = $request->get('target' );

				$route->save();
				
				return redirect( 'route/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Route updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('route/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Route Already Exist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}
}
