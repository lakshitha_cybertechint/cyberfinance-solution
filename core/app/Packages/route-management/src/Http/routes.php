<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'route', 'namespace' => 'RouteManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'route.add', 'uses' => 'RouteController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'route.edit', 'uses' => 'RouteController@editView'
      ]);

      Route::get('list', [
        'as' => 'route.list', 'uses' => 'RouteController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'route.list', 'uses' => 'RouteController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'route.add', 'uses' => 'RouteController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'route.edit', 'uses' => 'RouteController@edit'
      ]);

      Route::post('status', [
        'as' => 'route.status', 'uses' => 'RouteController@status'
      ]);

      Route::post('delete', [
        'as' => 'route.delete', 'uses' => 'RouteController@delete'
      ]);
    });
});