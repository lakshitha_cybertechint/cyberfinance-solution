<?php

namespace RouteManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Branch Model Class
 *
 */
class Route extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'routes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'name', 'day', 'target' ];


}
