@extends('layouts.back.master') @section('current_title','Update Customer')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('branch/list')}}">Customer Management</a></li>
       
        <li class="active">
            <span>Update Customer</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form">
                	{!!Form::token()!!}

                    <div class="form-group"><label class="col-sm-2 control-label">SHOP NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="shop_name" value="{{$curCustomer->shop_name}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ADDRESS 1</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="address1" value="{{$curCustomer->address1}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ADDRESS 2</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="address2" value="{{$curCustomer->address2}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">STREET</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="street" value="{{$curCustomer->street}}"></div>
                    </div>
                    {{--<div class="form-group"><label class="col-sm-2 control-label">REGION</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<select class="js-source-states" style="width: 100%" name="region">--}}
                                {{--@foreach($city as $key => $value)--}}
                                    {{--<option value="{{$value->id}}">{{$value->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group"><label class="col-sm-2 control-label">GEO LOCATION</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="geo_location" value="{{$curCustomer->geo_location}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ROUTE</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="route" value="{{$curCustomer->route}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">OWNER</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="owner" value="{{$curCustomer->owner}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">CONTACT</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="contact" value="{{$curCustomer->contact}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="category" value="{{$curCustomer->category}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">NOTES</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="notes" value="{{$curCustomer->notes}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PAYMENT</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="payment" value="{{$curCustomer->payment}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DURATION</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="duration" value="{{$curCustomer->duration}}"></div>
                    </div>
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();

		$("#form").validate({
            rules: {
                name: {
                    required: true
                  
                },
                code:{
                	required: true
                },

                email:{
                    email:true
                },
                tel:{
                	digits:true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
	
	
</script>
@stop