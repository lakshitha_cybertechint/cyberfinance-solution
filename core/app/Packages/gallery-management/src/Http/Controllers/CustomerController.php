<?php

namespace CustomerManage\Http\Controllers;

use App\Http\Controllers\Controller;
use CustomerManage\Models\Customer;
use BranchManage\Models\City;
use CustomerManage\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;


class CustomerController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Customer Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView() {
		$city = City::all();

		return view( 'customerManage::customer.add' )->with( [ [ 'city' => $city ] ] );
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to customer add
	 */
	public function add( CustomerRequest $request ) {

		$count = Customer::where( 'shop_name', '=', $request->get( 'shop_name' ) )->count();
		if ( $count == 0 ) {
			Customer::Create( array(
					'shop_name'    => $request->input( 'shop_name' ),
					'address1'     => $request->input( 'address1' ),
					'address2'     => $request->input( 'address2' ),
					'street'       => $request->input( 'street' ),
					'city_id'      => $request->input( 'region' ),
					'geo_location' => $request->input( 'geo_location' ),
					'route'        => $request->input( 'route' ),
					'owner'        => $request->input( 'owner' ),
					'contact'      => $request->input( 'contact' ),
					'category'     => $request->input( 'category' ),
					'notes'        => $request->input( 'notes' ),
					'payment'      => $request->input( 'payment' ),
					'duration'     => $request->input( 'duration' )
				)
			);

			return redirect( 'customer/add' )->with( [
				'success'         => true,
				'success.message' => 'Customer Created successfully!',
				'success.title'   => 'Well Done!'
			] );
		} else {

			return redirect( 'customer/add' )->with( [
				'error'         => true,
				'error.message' => 'Customer Already Exist!',
				'error.title'   => 'Duplicate!'
			] );
		}


	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView() {
		return view( 'customerManage::customer.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList( Request $request ) {
		if ( $request->ajax() ) {
			$data     = Customer::all();
			$jsonList = array();
			$i        = 1;
			foreach ( $data as $key => $customer ) {

				$dd = array();
				array_push( $dd, $i );

				if ( $customer->shop_name != "" ) {
					array_push( $dd, $customer->shop_name );
				} else {
					array_push( $dd, "-" );
				}
				if ( $customer->route != "" ) {
					array_push( $dd, $customer->route );
				} else {
					array_push( $dd, "-" );
				}
				if ( $customer->owner != "" ) {
					array_push( $dd, $customer->owner );
				} else {
					array_push( $dd, "-" );
				}
				if ( $customer->contact != "" ) {
					array_push( $dd, $customer->contact );
				} else {
					array_push( $dd, "-" );
				}


				$permissions = Permission::whereIn( 'name', [
					'customer.edit',
					'admin'
				] )->where( 'status', '=', 1 )->lists( 'name' );
				if ( Sentinel::hasAnyAccess( $permissions ) ) {
					array_push( $dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url( 'customer/edit/' . $customer->id ) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Customer"><i class="fa fa-pencil"></i></a></center>' );
				} else {
					array_push( $dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>' );
				}

				$permissions = Permission::whereIn( 'name', [
					'customer.delete',
					'admin'
				] )->where( 'status', '=', 1 )->lists( 'name' );
				if ( Sentinel::hasAnyAccess( $permissions ) ) {
					array_push( $dd, '<center><a href="#" class="branch-delete" data-id="' . $customer->id . '" data-toggle="tooltip" data-placement="top" title="Delete Customer"><i class="fa fa-trash-o"></i></a></center>' );
				} else {
					array_push( $dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>' );
				}

				array_push( $jsonList, $dd );
				$i ++;
			}

			return Response::json( array( 'data' => $jsonList ) );
		} else {
			return Response::json( array( 'data' => [] ) );
		}
	}


	/**
	 * Activate or Deactivate user
	 *
	 * @param  Request $request branch id with status to change
	 *
	 * @return json object with status of success or failure
	 */
	public function status( Request $request ) {
		if ( $request->ajax() ) {
			$id     = $request->input( 'id' );
			$status = $request->input( 'status' );

			$branch = Branch::find( $id );
			if ( $branch ) {
				$branch->status = $status;
				$branch->save();

				return response()->json( [ 'status' => 'success' ] );
			} else {
				return response()->json( [ 'status' => 'invalid_id' ] );
			}
		} else {
			return response()->json( [ 'status' => 'not_ajax' ] );
		}
	}

	/**
	 * Delete a branch
	 *
	 * @param  Request $request branch id
	 *
	 * @return Json            json object with status of success or failure
	 */
	public function delete( Request $request ) {
		if ( $request->ajax() ) {
			$id = $request->input( 'id' );

			$customer = Customer::find( $id );
			if ( $customer ) {
				$customer->delete();

				return response()->json( [ 'status' => 'success' ] );
			} else {
				return response()->json( [ 'status' => 'invalid_id' ] );
			}
		} else {
			return response()->json( [ 'status' => 'not_ajax' ] );
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView( $id ) {
		$curCustomer = Customer::find( $id );
		$city      = City::get();
		if ( $curCustomer ) {
			return view( 'customerManage::customer.edit' )->with( [
				'curCustomer' => $curCustomer,

			] );
		} else {
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit( CustomerRequest $request, $id ) {


		$count = Customer::where( 'id', '!=', $id )->where( 'shop_name', '=', $request->get( 'shop_name' ) )->count();

		if ( $count == 0 ) {
			$customerOld       = Customer::where( 'id', $id )->take( 1 )->get();
			$customer          = $customerOld[0];


			$customer->shop_name    = $request->get( 'shop_name' );
			$customer->address1    = $request->get( 'address1' );
			$customer->address2    = $request->get( 'address2' );
			$customer->street    = $request->get( 'street' );
			$customer->region    = $request->get( 'region' );
			$customer->geo_location    = $request->get( 'geo_location' );
			$customer->route    = $request->get( 'route' );
			$customer->owner    = $request->get( 'owner' );
			$customer->contact    = $request->get( 'contact' );
			$customer->category    = $request->get( 'category' );
			$customer->notes    = $request->get( 'notes' );
			$customer->payment    = $request->get( 'payment' );
			$customer->duration    = $request->get( 'duration' );

			$customer->save();

			return redirect( 'customer/edit/' . $id )->with( [
				'success'         => true,
				'success.message' => 'Customer updated successfully!',
				'success.title'   => 'Good Job!'
			] );
		} else {
			return redirect( 'customer/edit/' . $id )->with( [
				'error'         => true,
				'error.message' => 'Customer Already Exsist!',
				'error.title'   => 'Duplicate!'
			] );
		}


	}
}
