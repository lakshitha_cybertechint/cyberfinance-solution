<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'customer', 'namespace' => 'CustomerManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'customer.add', 'uses' => 'CustomerController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'customer.edit', 'uses' => 'CustomerController@editView'
      ]);

      Route::get('list', [
        'as' => 'customer.list', 'uses' => 'CustomerController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'customer.list', 'uses' => 'CustomerController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'customer.add', 'uses' => 'CustomerController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'customer.edit', 'uses' => 'CustomerController@edit'
      ]);

      Route::post('status', [
        'as' => 'customer.status', 'uses' => 'CustomerController@status'
      ]);

      Route::post('delete', [
        'as' => 'customer.delete', 'uses' => 'CustomerController@delete'
      ]);
    });
});