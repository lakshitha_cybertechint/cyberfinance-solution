<?php

namespace CustomerManage\Models;

use Illuminate\Database\Eloquent\Model;


class Customer extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_customer';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'shop_name',
		'address1',
		'address2',
		'street',
		'region',
		'geo_location',
		'route',
		'owner',
		'contact',
		'category',
		'notes',
		'payment',
		'duration'
	];

	public function getCity()
	{
		return $this->belongsTo('BranchManage\Models\City', 'city_id', 'id');
	}


}
