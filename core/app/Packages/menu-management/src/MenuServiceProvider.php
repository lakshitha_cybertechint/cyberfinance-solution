<?php

namespace MenuManage;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'menuManage');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('menumanage', function($app){
            return new MenuManage;
        });
    }
}
