<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'branch', 'namespace' => 'BranchManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'branch.add', 'uses' => 'BranchController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'branch.edit', 'uses' => 'BranchController@editView'
      ]);

      Route::get('list', [
        'as' => 'branch.list', 'uses' => 'BranchController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'branch.list', 'uses' => 'BranchController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'branch.add', 'uses' => 'BranchController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'branch.edit', 'uses' => 'BranchController@edit'
      ]);

      Route::post('status', [
        'as' => 'branch.status', 'uses' => 'BranchController@status'
      ]);

      Route::post('delete', [
        'as' => 'branch.delete', 'uses' => 'BranchController@delete'
      ]);
    });
});